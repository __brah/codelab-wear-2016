package com.worldline.android.twitterwall.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.worldline.android.twitterwall.R;
import com.worldline.android.twitterwall.async.DownloadImageAsyncTask;
import com.worldline.android.twitterwall.components.ImageMemoryCache;
import com.worldline.android.twitterwall.database.WLTwitterDatabaseManager;
import com.worldline.android.twitterwall.interfaces.TweetListener;
import com.worldline.android.twitterwall.pojo.Tweet;

public class TweetsCursorAdapter extends CursorAdapter implements OnClickListener {

	// The cache for images
	private final ImageMemoryCache mImageMemoryCache;

	// The listener for events
	private TweetListener mListener;

	public TweetsCursorAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);

		// Instantiate our cache
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 16;
		mImageMemoryCache = new ImageMemoryCache(cacheSize);
	}

	public void setTweetListener(TweetListener listener){
		mListener = listener;
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// Retrieve the ViewHolder
		final ViewHolder holder = (ViewHolder) view.getTag();

		// Get the tweet from cursor
		final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);

		// Set the user name
		holder.name.setText(tweet.user.name);

		// Set the alias
		holder.alias.setText("@"+tweet.user.screenName);

		// Set the text
		holder.text.setText(tweet.text);

		// Register a listener to handle the click on the button
		// And keep track of the position in the tag of the button
		holder.button.setTag(tweet);
		holder.button.setOnClickListener(this);

		// Display the images
		final Bitmap image = mImageMemoryCache.getBitmapFromMemCache(tweet.user.profileImageUrl);
		if (null == image){
			new DownloadImageAsyncTask(holder.image, mImageMemoryCache).execute(tweet.user.profileImageUrl);	
		} else {
			holder.image.setImageBitmap(image);
		}
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup container) {
		final View view = LayoutInflater.from(context).inflate(R.layout.tweet_listitem, null);

		// Instantiate the ViewHolder
		final ViewHolder holder = new ViewHolder(view);

		// Set as tag to the convertView to retrieve it easily
		view.setTag(holder);

		return view;
	}

	public class ViewHolder {
		public ImageView image;
		public TextView name;
		public TextView alias;
		public TextView text;
		public Button button;

		public ViewHolder(View view){
			image = (ImageView) view.findViewById(R.id.tweetListItemImageView);
			name = (TextView) view.findViewById(R.id.tweetListItemNameTextView);
			alias = (TextView) view.findViewById(R.id.tweetListItemAliasTextView);
			text = (TextView) view.findViewById(R.id.tweetListItemTweetTextView);
			button = (Button) view.findViewById(R.id.tweetListItemButton);
		}
	}

	@Override
	public void onClick(View view) {
		// If we have a listener set, call the retweet method
		if (null != mListener){
			final Tweet tweet = (Tweet) view.getTag();
			mListener.onRetweet(tweet);
		}
	}
}
