package com.worldline.android.twitterwall.services;

import java.util.List;

import com.worldline.android.twitterwall.async.RetrieveTweetsAsyncTask;
import com.worldline.android.twitterwall.database.WLTwitterDatabaseManager;
import com.worldline.android.twitterwall.interfaces.TweetChangeListener;
import com.worldline.android.twitterwall.pojo.Tweet;
import com.worldline.android.twitterwall.utils.Constants;
import com.worldline.android.twitterwall.utils.PreferenceUtils;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

public class TweetService extends Service implements TweetChangeListener {

	// Our Binder to pass to Activity
	@SuppressWarnings("unused")
	private final IBinder mBinder = new WLTwitterBinder();

	@Override
	public IBinder onBind(Intent intent) {
//		return mBinder;
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		new RetrieveTweetsAsyncTask(this).execute(PreferenceUtils.getLogin());
		return Service.START_NOT_STICKY;
	}

	public class WLTwitterBinder extends Binder {
		TweetService getService() {
			return TweetService.this;
		}
		
		// Define some methods here to let your Activity interact with the service
	}

	@Override
	public void onTweetRetrieved(List<Tweet> tweets) {
		// The number of tweet inserted in database
		int nbTweetInserted = 0;
		
		// Insert all tweets
		for (Tweet tweet : tweets){
			final int id = WLTwitterDatabaseManager.insertTweet(tweet);
			if (id > -1){
				nbTweetInserted++;
			}
		}
		
		// If we inserted at least one tweet, broadcast a message
		if (nbTweetInserted > 0){
			final Intent newTweetsIntent = new Intent(Constants.General.ACTION_NEW_TWEETS);
			final Bundle extras = new Bundle();
			extras.putInt(Constants.General.ACTION_NEW_TWEETS_EXTRA_NB_TWEETS, nbTweetInserted);
			newTweetsIntent.putExtras(extras);
			sendBroadcast(newTweetsIntent);
		}
		
		stopSelf();
	}
}
