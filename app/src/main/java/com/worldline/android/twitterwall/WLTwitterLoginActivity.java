package com.worldline.android.twitterwall;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.worldline.android.twitterwall.utils.Constants;
import com.worldline.android.twitterwall.utils.PreferenceUtils;

public class WLTwitterLoginActivity extends AppCompatActivity {

	// The EditText in which the user type login
	private EditText mLoginEditText;

	// The EditText in which the user type password
	private EditText mPasswordEditText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setContentView(R.layout.activity_login);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo);

		// Keep a reference to the EditText
		mLoginEditText = (EditText) findViewById(R.id.loginEditText);
		mPasswordEditText = (EditText) findViewById(R.id.passwordEditText);

		
		final String storedLogin = PreferenceUtils.getLogin();
		final String storedPassword = PreferenceUtils.getPassword();
		if ((!TextUtils.isEmpty(storedLogin)) && (!TextUtils.isEmpty(storedPassword))){
			final Intent homeIntent = getHomeActivityIntent(storedLogin);
			startActivity(homeIntent);
		}
	}

	 public void signInClick(View v){

		TextInputLayout tilLogin = (TextInputLayout) findViewById(R.id.loginLayoutEditText);
        TextInputLayout tilPassword = (TextInputLayout) findViewById(R.id.passwordLayoutEditText);
 
        tilLogin.setErrorEnabled(false);
        tilPassword.setErrorEnabled(false);

		// Check if a login is set
		if (TextUtils.isEmpty(mLoginEditText.getText())){
			// Display a Toast to the user
			tilLogin.setErrorEnabled(true);
            tilLogin.setError("You need to enter a name");
		}

		// Check if a password is set
		if (TextUtils.isEmpty(mPasswordEditText.getText())){
			 // Display a Toast to the user
            tilPassword.setErrorEnabled(true);
            tilPassword.setError("You need to enter a password");
		}

		if(TextUtils.isEmpty(mPasswordEditText.getText()) || TextUtils.isEmpty(mLoginEditText.getText())){
			return;
		}

		// Before launching the second Activity, just save the values in SharedPreferences
		PreferenceUtils.setLogin(mLoginEditText.getText().toString());
		PreferenceUtils.setPassword(mPasswordEditText.getText().toString());
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				// Do some stuff

				// Then call this method on an Activity to change GUI
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// Do some GUI changes
					}
				});
			}
		}).start();
		
		// Here we are, a login and password are set, try to login
		// For now just launch the second activity, to do that create an Intent
		final Intent homeIntent = getHomeActivityIntent(mLoginEditText.getText().toString());
		startActivity(homeIntent);
	}

	private Intent getHomeActivityIntent(String userName){
		final Intent intent = new Intent(this, WLTwitterActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		final Bundle extras = new Bundle();
		extras.putString(Constants.Login.EXTRA_LOGIN, userName);
		intent.putExtras(extras);
		return intent;
	}



}

