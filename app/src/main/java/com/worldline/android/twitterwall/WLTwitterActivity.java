package com.worldline.android.twitterwall;

import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.worldline.android.twitterwall.database.WLTwitterDatabaseManager;
import com.worldline.android.twitterwall.interfaces.TweetListener;
import com.worldline.android.twitterwall.pojo.Tweet;
import com.worldline.android.twitterwall.services.TweetService;
import com.worldline.android.twitterwall.ui.fragments.TweetFragment;
import com.worldline.android.twitterwall.ui.fragments.TweetsFragment;
import com.worldline.android.twitterwall.utils.Constants;
import com.worldline.android.twitterwall.utils.NotificationsUtils;
import com.worldline.android.twitterwall.utils.PreferenceUtils;

import java.util.Calendar;
import java.util.HashSet;

public class WLTwitterActivity extends AppCompatActivity implements TweetListener {

    // The PendingIntent to call service
    private PendingIntent mServicePendingIntent;

    // The receiver for new tweets
    private NewTweetsReceiver mReceiver;

    GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wltwitter);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo);

        // Retrieve the login passed as parameter
        final Intent intent = getIntent();
        if (null != intent) {
            final Bundle extras = intent.getExtras();
            if ((null != extras) && (extras.containsKey(Constants.Login.EXTRA_LOGIN))) {
                // Retrieve the login
                final String login = extras.getString(Constants.Login.EXTRA_LOGIN);

                // Set as ActionBar subtitle
                getSupportActionBar().setSubtitle("@" + login);
            }
        }

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(R.id.container, new TweetsFragment()).commit();
        }

        startGoogleApiClient();


    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register a receiver when new Tweets are downloaded
        mReceiver = new NewTweetsReceiver();
        registerReceiver(mReceiver, new IntentFilter(Constants.General.ACTION_NEW_TWEETS));

        // Schedule service to run every xx seconds (defined in Constants.Twitter.POLLING_DELAY)
        final Calendar cal = Calendar.getInstance();
        final Intent serviceIntent = new Intent(this, TweetService.class);
        mServicePendingIntent = PendingIntent.getService(this, 0, serviceIntent, 0);
        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), Constants.Twitter.POLLING_DELAY, mServicePendingIntent);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister our receiver
        unregisterReceiver(mReceiver);
        mReceiver = null;

        // Cancel the service repetition
        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(mServicePendingIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.wltwitter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.actionLogout) {
            // Erase login and password in Preferences
            PreferenceUtils.setLogin(null);
            PreferenceUtils.setPassword(null);

            // Erase all datas in database
            WLTwitterDatabaseManager.dropDatabase();

            // Finish this activity, and go back to LoginActivity
            Intent it = new Intent(this, WLTwitterLoginActivity.class);
            it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(it);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRetweet(Tweet tweet) {

        Snackbar
                .make(findViewById(R.id.contentView), tweet.text, Snackbar.LENGTH_LONG)
                .show();

    }

    @Override
    public void onViewTweet(Tweet tweet) {
        final FragmentTransaction transaction = getFragmentManager().beginTransaction();

        final TweetFragment fragment = new TweetFragment();
        final Bundle bundle = new Bundle();
        bundle.putString(Constants.Tweet.EXTRA_IMAGEURL, tweet.user.profileImageUrl);
        bundle.putString(Constants.Tweet.EXTRA_NAME, tweet.user.name);
        bundle.putString(Constants.Tweet.EXTRA_ALIAS, tweet.user.screenName);
        bundle.putString(Constants.Tweet.EXTRA_TEXT, tweet.text);
        fragment.setArguments(bundle);

        fragment.show(getSupportFragmentManager(),"TweetPopUp");


    }

    private class NewTweetsReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final int nbNewTweets = intent.getExtras().getInt(Constants.General.ACTION_NEW_TWEETS_EXTRA_NB_TWEETS);

            // Display a notification
            NotificationsUtils.displayNewTweetsNotification(nbNewTweets, true, true);
        }

    }


    private void startGoogleApiClient() {

         mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d("phoneapp", "onConnected: " + connectionHint);
                        sendMessage();
                    }

                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d("phoneapp", "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d("phoneapp", "onConnectionFailed: " + result);
                    }
                })
                // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();

        mGoogleApiClient.connect();

    }

    private void sendMessage() {

        new AsyncTask<String,Void,Boolean>(){

            @Override
            protected Boolean doInBackground(String[] params) {

                Wearable.MessageApi.sendMessage(mGoogleApiClient, getFirstNodes(),
                        "/a_message_path", "hello watch world !".getBytes()).setResultCallback(
                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(MessageApi.SendMessageResult sendMessageResult) {
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    Log.d("phoneapp", "Failed to send message" );
                                }
                            }
                        });
                return true;
            }
        }.execute();

    }

    private String getFirstNodes( ) {
        HashSet<String> results = new HashSet<String>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
        for (Node node : nodes.getNodes()) {
            Log.d("phoneapp", "First node ID"+ node.getId());
            return node.getId();

        }
        return null;
    }
}
