package com.worldline.android.twitterwall.async;

import android.os.AsyncTask;

import java.util.List;

import com.worldline.android.twitterwall.helpers.TwitterHelper;
import com.worldline.android.twitterwall.interfaces.TweetChangeListener;
import com.worldline.android.twitterwall.pojo.Tweet;

public class RetrieveTweetsAsyncTask extends AsyncTask<String, Void, List<Tweet>>{

	// A reference to the listener
	private TweetChangeListener mListener;
	
	public RetrieveTweetsAsyncTask(TweetChangeListener listener){
		mListener = listener;
	}
	
	@Override
	protected List<Tweet> doInBackground(String... params) {
		if ((null != params) && (params.length > 0)){
			return TwitterHelper.getTweetsOfUser(params[0]);
		}
		return null;
	}

	@Override
	protected void onPostExecute(List<Tweet> result) {
		if (null != mListener){
			mListener.onTweetRetrieved(result);
		}
	}
	
}
