package com.worldline.android.twitterwall.async;

import android.os.AsyncTask;

import java.util.List;

import com.worldline.android.twitterwall.database.WLTwitterDatabaseManager;
import com.worldline.android.twitterwall.pojo.Tweet;

public class StoreTweetsAsyncTask extends AsyncTask<Void, Void, Void> {

	// The Tweets to store
	private final List<Tweet> mTweets;
	
	public StoreTweetsAsyncTask (List<Tweet> tweets){
		mTweets = tweets;
	}
	
	@Override
	protected Void doInBackground(Void... params) {
		for (Tweet tweet : mTweets){
			WLTwitterDatabaseManager.insertTweet(tweet);
		}
		return null;
	}

}
