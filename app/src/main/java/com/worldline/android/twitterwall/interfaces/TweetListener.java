package com.worldline.android.twitterwall.interfaces;

import com.worldline.android.twitterwall.pojo.Tweet;

public interface TweetListener {

	public void onRetweet(Tweet tweet);
	public void onViewTweet(Tweet tweet);
	
}
