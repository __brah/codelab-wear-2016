package com.worldline.android.twitterwall.interfaces;

import java.util.List;

import com.worldline.android.twitterwall.pojo.Tweet;

public interface TweetChangeListener {

	public void onTweetRetrieved(List<Tweet> tweets);
	
}
