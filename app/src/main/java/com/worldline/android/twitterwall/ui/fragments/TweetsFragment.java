package com.worldline.android.twitterwall.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.worldline.android.twitterwall.R;
import com.worldline.android.twitterwall.adapters.TweetsCursorRecyclerAdapter;
import com.worldline.android.twitterwall.database.WLTwitterDatabaseContract;
import com.worldline.android.twitterwall.interfaces.RecyclerItemClickListener;
import com.worldline.android.twitterwall.interfaces.TweetListener;
import com.worldline.android.twitterwall.pojo.Tweet;

public class TweetsFragment extends Fragment implements  LoaderManager.LoaderCallbacks<Cursor> {

	// Keep a reference to the ListView
	//private ListView mListView;
	private RecyclerView mRecyclerView;
	
	// Keep a reference to our Activiyt (implementing TweetListener)
	private TweetListener mListener;
	
	// The cursor loader to load tweets
	private static final int TWEET_CURSOR_LOADER = 1;
	
	// Our adapter
	//private TweetsCursorAdapter mAdapter;
	private TweetsCursorRecyclerAdapter mAdapter;
	
	public TweetsFragment() {}

	private SwipeRefreshLayout aSwipeRefreshLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_wltwitter, container, false);
		
		// Get the ListView
		//mListView = (ListView) rootView.findViewById(R.id.tweetsListView);
		mRecyclerView = (RecyclerView) rootView.findViewById(R.id.tweetsRecyclerView);
		mRecyclerView.setHasFixedSize(true);
        // Set adapter with no elements to let the ListView display the empty view
        mAdapter = new TweetsCursorRecyclerAdapter(getActivity(), null);
        mAdapter.setTweetListener(mListener);
		mRecyclerView.setAdapter(mAdapter);

		// use a linear layout manager
		LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
		mRecyclerView.setLayoutManager(mLayoutManager);

        // Add a listener when an item is clicked
		mRecyclerView.addOnItemTouchListener( new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
			@Override
			public void onItemClick(View view, int position) {
				if (null != mListener){
					final TweetsCursorRecyclerAdapter.CustomViewHolder holder = (TweetsCursorRecyclerAdapter.CustomViewHolder) view.getTag();
					final Tweet tweet = (Tweet) holder.button.getTag();
					mListener.onViewTweet(tweet);
				}
			}
		}));
        
        // Initialize the cursor loader

		aSwipeRefreshLayout =(SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);
		SwipeRefreshLayout.OnRefreshListener onRefreshListener= new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				getLoaderManager().initLoader(TWEET_CURSOR_LOADER, null, TweetsFragment.this);
			}
		};
		aSwipeRefreshLayout.setOnRefreshListener(onRefreshListener);

		aSwipeRefreshLayout.setRefreshing(true);
		aSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getActivity(), R.color.blue_twitter));
		onRefreshListener.onRefresh();

        
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if (activity instanceof TweetListener){
			mListener = (TweetListener) activity;
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int loaderId, Bundle bundle) {
		if (loaderId == TWEET_CURSOR_LOADER){
			return new CursorLoader(
                    getActivity(),  
                    WLTwitterDatabaseContract.TWEETS_URI,
                    WLTwitterDatabaseContract.PROJECTION_FULL,
                    null, null, WLTwitterDatabaseContract.ORDER_BY_DATE_CREATED_TIMESTAMP_DESCENDING);      
		}
		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if (null != mAdapter){
			mAdapter.changeCursor(cursor);
		}
		aSwipeRefreshLayout.setRefreshing(false);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {

	}
	
}
