package com.worldline.android.twitterwall.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.worldline.android.twitterwall.R;
import com.worldline.android.twitterwall.async.DownloadImageAsyncTask;
import com.worldline.android.twitterwall.components.ImageMemoryCache;
import com.worldline.android.twitterwall.database.WLTwitterDatabaseManager;
import com.worldline.android.twitterwall.interfaces.TweetListener;
import com.worldline.android.twitterwall.pojo.Tweet;

public class TweetsCursorRecyclerAdapter extends CursorRecyclerAdapter<TweetsCursorRecyclerAdapter.CustomViewHolder> implements OnClickListener {

	// The cache for images
	private final ImageMemoryCache mImageMemoryCache;
	private final LayoutInflater layoutInflater;
	private final Context aContext;

	// The listener for events
	private TweetListener mListener;

	public TweetsCursorRecyclerAdapter(Context context, Cursor cursor) {
		super(context, cursor);

		this.layoutInflater = LayoutInflater.from(context);

		// Instantiate our cache
		final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
		final int cacheSize = maxMemory / 16;
		mImageMemoryCache = new ImageMemoryCache(cacheSize);

		aContext=context;

	}


	public void setTweetListener(TweetListener listener){
		mListener = listener;
	}

	@Override
	public void onBindViewHolder(CustomViewHolder viewHolder, Cursor cursor) {
		// Get the tweet from cursor
		final Tweet tweet = WLTwitterDatabaseManager.tweetFromCursor(cursor);

		// Set the user name
		viewHolder.name.setText(tweet.user.name);

		// Set the alias
		viewHolder.alias.setText("@"+tweet.user.screenName);

		// Set the text
		viewHolder.text.setText(tweet.text);

		// Register a listener to handle the click on the button
		// And keep track of the position in the tag of the button
		viewHolder.button.setTag(tweet);
		viewHolder.button.setOnClickListener(this);

		// Display the images
		final Bitmap image = mImageMemoryCache.getBitmapFromMemCache(tweet.user.profileImageUrl);
		if (null == image){
			new DownloadImageAsyncTask(viewHolder.image, mImageMemoryCache).execute(tweet.user.profileImageUrl);
		} else {
			viewHolder.image.setImageBitmap(image);
		}
	}


	@Override
	public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

		final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweet_listitem, null);

		// Instantiate the ViewHolder
		CustomViewHolder aCustomViewHolder = new CustomViewHolder(view);
		view.setTag(aCustomViewHolder);
		return aCustomViewHolder;

	}



	public class CustomViewHolder extends RecyclerView.ViewHolder{


		public ImageView image;
		public TextView name;
		public TextView alias;
		public TextView text;
		public Button button;


		public CustomViewHolder(View itemView) {
			super(itemView);

			image = (ImageView) itemView.findViewById(R.id.tweetListItemImageView);
			name = (TextView) itemView.findViewById(R.id.tweetListItemNameTextView);
			alias = (TextView) itemView.findViewById(R.id.tweetListItemAliasTextView);
			text = (TextView) itemView.findViewById(R.id.tweetListItemTweetTextView);
			button = (Button) itemView.findViewById(R.id.tweetListItemButton);
		}

	}

	@Override
	public void onClick(View view) {
		// If we have a listener set, call the retweet method
		if (null != mListener){
			final Tweet tweet = (Tweet) view.getTag();
			mListener.onRetweet(tweet);
		}
	}
}
