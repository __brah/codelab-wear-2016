package com.worldline.android.twitterwall;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;

import java.util.List;

/**
 * Created by A187839 on 28/11/2016.
 */

public class SampleGridPagerAdapter extends FragmentGridPagerAdapter {

    private final Context mContext;
    private List mRows;

    // A simple container for static data in each page
    private static class Page {

        String title;
        String text;
        int iconRes;

        public Page(String title,String text,int iconRes) {
            this.title = title;
            this.text = text;
            this.iconRes = iconRes;
        }
    }

    // Create a static set of pages in a 2D array
    private final Page[][] PAGES = {
            {
                    new Page("X0 Y0","lorem ipsum 00...",R.mipmap.ic_launcher),
                    new Page("X1 Y0","lorem ipsum 10...",R.mipmap.ic_launcher)
            },
            {
                    new Page("X0 Y1","lorem ipsum 01...",R.mipmap.ic_launcher),
                    new Page("X1 Y1","lorem ipsum 11...",R.mipmap.ic_launcher)
            }
    };


    public SampleGridPagerAdapter(Context ctx, FragmentManager fm) {
        super(fm);
        mContext = ctx;
    }

    @Override
    public int getRowCount() {
        return PAGES.length;
    }
    @Override
    public int getColumnCount(int rowNum) {
        return PAGES[rowNum].length;
    }
    @Override
    public Fragment getFragment(int row, int col) {
        Page page = PAGES[row][col];
        String title = page.title;
        String text = page.text;
        CardFragment fragment = CardFragment.create(title, text, page.iconRes);

        return fragment;
    }}




